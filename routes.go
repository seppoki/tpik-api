package main


import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	database "gitlab.com/seppoki/tpik-api/database/compiled"
)

type QueryParameters struct {
	terms    string
	offsetId int32
	limit    int32
	timespan int32
	ids      []int32
	lang     []string
	sources  []string
	order    string
}

func ParseQueryParameters(v url.Values) QueryParameters {
	p := QueryParameters{}

	p.terms = v.Get("terms")
	offsetId, err := strconv.Atoi(v.Get("offsetId"))
	if err != nil {
		p.offsetId = 0
	} else {
		p.offsetId = int32(offsetId)
	}

	limit, err := strconv.Atoi(v.Get("limit"))
	if err != nil {
		p.limit = 0
	} else {
		p.limit = int32(limit)
	}

	timespan, err := strconv.Atoi(v.Get("timespan"))
	if err != nil {
		p.timespan = 0
	} else {
		p.timespan = int32(timespan)
	}

	qids := v.Get("ids")
	sids := strings.Split(qids, ",")
	ids := []int32{}
	for _, sv := range sids {
		i, err := strconv.Atoi(sv)
		if err == nil {
			ids = append(ids, int32(i))
		}
	}
	if len(ids) > 0 {
		p.ids = ids
	} else {
		p.ids = nil
	}

	qlang := v.Get("lang")
	lang := strings.Split(qlang, ",")
	if len(lang) > 0 {
		p.lang = lang
	} else {
		p.lang = nil
	}

	qsources := v.Get("sources")
	sources := strings.Split(qsources, ",")
	if len(sources) > 0 {
		p.sources = sources
	} else {
		p.sources = nil
	}

	p.order = v.Get("order")

	return p
}

type SearchResponse struct {
	Data *[]Vacancy `json:"data"`
}

func Search(w http.ResponseWriter, r *http.Request, ctx *context.Context, queries *database.Queries) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	p := ParseQueryParameters(r.URL.Query())

	if p.terms == "" || p.limit == 0 || p.timespan == 0 || len(p.lang) == 0 || len(p.sources) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	results := make([]Vacancy, 0)
	if p.order == "id" {
		if p.offsetId == 0 {
			param := database.GetVacanciesOrderIdParams{
				Terms:       p.terms,
				Lang:        p.lang,
				Timespan:    p.timespan,
				ResultLimit: p.limit,
				Sources:     p.sources,
			}
			rows, err := queries.GetVacanciesOrderId(*ctx, param)
			if err != nil {
				fmt.Println(err)
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			for _, v := range rows {
				results = append(results, Vacancy(v))
			}
		} else {
			param := database.GetVacanciesOrderIdMoreParams{
				Terms:       p.terms,
				OffsetId:    p.offsetId,
				Lang:        p.lang,
				Timespan:    p.timespan,
				ResultLimit: p.limit,
				Sources:     p.sources,
			}
			rows, err := queries.GetVacanciesOrderIdMore(*ctx, param)
			if err != nil {
				fmt.Println(err)
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			for _, v := range rows {
				results = append(results, Vacancy(v))
			}
		}
	} else if p.order == "rank" {
		fmt.Println(p.ids)
		if len(p.ids) == 0 {
			param := database.GetVacanciesOrderRankParams{
				Terms:       p.terms,
				Lang:        p.lang,
				Timespan:    p.timespan,
				ResultLimit: p.limit,
				Sources:     p.sources,
			}
			fmt.Println(param)
			rows, err := queries.GetVacanciesOrderRank(*ctx, param)
			if err != nil {
				fmt.Println(err)
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			for _, v := range rows {
				results = append(results, Vacancy(v))
			}
		} else {
			param := database.GetVacanciesOrderRankMoreParams{
				Terms:       p.terms,
				Lang:        p.lang,
				Ids:         p.ids,
				Timespan:    p.timespan,
				ResultLimit: p.limit,
				Sources:     p.sources,
			}
			fmt.Println(param)
			rows, err := queries.GetVacanciesOrderRankMore(*ctx, param)
			if err != nil {
				fmt.Println(err)
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			for _, v := range rows {
				results = append(results, Vacancy(v))
			}
		}
	}

	response := SearchResponse{Data: &results}
	vacanciesBytes, err := json.MarshalIndent(response, "", "\t")
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(vacanciesBytes); err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func Latest(w http.ResponseWriter, r *http.Request, ctx *context.Context, queries *database.Queries) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	qlimit := r.URL.Query().Get("limit")
	limit, err := strconv.Atoi(qlimit)
	if err != nil {
		limit = 50
	}
	results := make([]Vacancy, 0)
	rows, err := queries.GetVacanciesLatest(*ctx, int32(limit))
	if err != nil {
			fmt.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		for _, v := range rows {
			results = append(results, Vacancy(v))
		}


	response := SearchResponse{Data: &results}
	vacanciesBytes, err := json.MarshalIndent(response, "", "\t")
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(vacanciesBytes); err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}
