create table vacancies (
    id integer NOT NULL,
    url text NOT NULL,
    header text NOT NULL,
    ts timestamp with time zone DEFAULT now() NOT NULL,
    lang text,
    contents text NOT NULL,
    source text NOT NULL,
    ts_simple tsvector,
    ts_finnish tsvector,
    ts_english tsvector
);
