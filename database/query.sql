-- name: GetVacanciesOrderId :many
SELECT id,
url,
header,
source,
ts,
lang,
ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))) as rank_simple,
ts_headline('simple', contents, to_tsquery('simple', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_simple,
ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))) as rank_finnish,
ts_headline('finnish', contents, to_tsquery('finnish', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_finnish,
ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms))) as rank_english,
ts_headline('english', contents, to_tsquery('english', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_english
FROM vacancies
WHERE lang = ANY(sqlc.arg(lang)::char(3)[])
AND source = ANY(sqlc.arg(sources)::text[])
AND ts > current_date - make_interval(days => sqlc.arg(timespan))
AND (ts_simple @@ to_tsquery('simple', sqlc.arg(terms))
    OR ts_finnish @@ to_tsquery('finnish', sqlc.arg(terms))
    OR ts_english @@ to_tsquery('english', sqlc.arg(terms)))
ORDER BY id DESC
LIMIT sqlc.arg('resultLimit');

-- name: GetVacanciesOrderIdMore :many
SELECT id,
url,
header,
source,
ts,
lang,
ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))) as rank_simple,
ts_headline('simple', contents, to_tsquery('simple', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_simple,
ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))) as rank_finnish,
ts_headline('finnish', contents, to_tsquery('finnish', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_finnish,
ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms))) as rank_english,
ts_headline('english', contents, to_tsquery('english', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_english
FROM vacancies
WHERE id < sqlc.arg('offsetId')
AND source = ANY(sqlc.arg(sources)::text[])
AND lang = ANY(sqlc.arg(lang)::char(3)[])
AND ts > current_date - make_interval(days => sqlc.arg(timespan))
AND (ts_simple @@ to_tsquery('simple', sqlc.arg(terms))
    OR ts_finnish @@ to_tsquery('finnish', sqlc.arg(terms))
    OR ts_english @@ to_tsquery('english', sqlc.arg(terms)))
ORDER BY id DESC
LIMIT sqlc.arg('resultLimit');

-- name: GetVacanciesOrderRank :many
SELECT id,
url,
header,
source,
ts,
lang,
ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))) as rank_simple,
ts_headline('simple', contents, to_tsquery('simple', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_simple,
ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))) as rank_finnish,
ts_headline('finnish', contents, to_tsquery('finnish', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_finnish,
ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms))) as rank_english,
ts_headline('english', contents, to_tsquery('english', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_english
FROM vacancies
WHERE lang = ANY(sqlc.arg(lang)::char(3)[])
AND source = ANY(sqlc.arg(sources)::text[])
AND ts > current_date - make_interval(days => sqlc.arg(timespan))
AND (ts_simple @@ to_tsquery('simple', sqlc.arg(terms))
    OR ts_finnish @@ to_tsquery('finnish', sqlc.arg(terms))
    OR ts_english @@ to_tsquery('english', sqlc.arg(terms)))
ORDER BY greatest(
    ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))),
    ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))),
    ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms)))
) DESC
LIMIT sqlc.arg('resultLimit');

-- name: GetVacanciesOrderRankMore :many
SELECT id,
url,
header,
source,
ts,
lang,
ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))) as rank_simple,
ts_headline('simple', contents, to_tsquery('simple', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_simple,
ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))) as rank_finnish,
ts_headline('finnish', contents, to_tsquery('finnish', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_finnish,
ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms))) as rank_english,
ts_headline('english', contents, to_tsquery('english', sqlc.arg(terms)), 'MaxFragments=30, MaxWords=20, MinWords=5')::text as headline_english
FROM vacancies
WHERE lang = ANY(sqlc.arg(lang)::char(3)[])
AND source = ANY(sqlc.arg(sources)::text[])
AND NOT id = ANY(sqlc.arg(ids)::int[])
AND ts > current_date - make_interval(days => sqlc.arg(timespan))
AND (ts_simple @@ to_tsquery('simple', sqlc.arg(terms))
    OR ts_finnish @@ to_tsquery('finnish', sqlc.arg(terms))
    OR ts_english @@ to_tsquery('english', sqlc.arg(terms)))
ORDER BY greatest(
    ts_rank(ts_simple, to_tsquery('simple', sqlc.arg(terms))),
    ts_rank(ts_finnish, to_tsquery('finnish', sqlc.arg(terms))),
    ts_rank(ts_english, to_tsquery('english', sqlc.arg(terms)))
) DESC
LIMIT sqlc.arg('resultLimit');

-- name: GetVacanciesLatest :many
SELECT id,
url,
header,
source,
ts,
lang,
0::real as rank_simple,
''::text as headline_simple,
0::real as rank_finnish,
''::text as headline_finnish,
0::real as rank_english,
''::text as headline_english
FROM vacancies
ORDER BY id DESC
LIMIT sqlc.arg('resultLimit');
