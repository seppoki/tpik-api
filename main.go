package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"os"

	_ "github.com/lib/pq"
	database "gitlab.com/seppoki/tpik-api/database/compiled"
)

func run() error {
	postgresUrl := os.Getenv("POSTGRES_URL")
	fmt.Println(postgresUrl)

	ctx := context.Background()

	db, err := sql.Open("postgres", postgresUrl)
	if err != nil {
		return err
	}
	queries := database.New(db)

	http.HandleFunc("/search", func(w http.ResponseWriter, r *http.Request) {
		Search(w, r, &ctx, queries)
	})
	http.HandleFunc("/latest", func(w http.ResponseWriter, r *http.Request) {
		Latest(w, r, &ctx, queries)
	})
	bindAddress := os.Getenv("BIND_ADDRESS")
	fmt.Println(bindAddress)
	return http.ListenAndServe(bindAddress, nil)
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
