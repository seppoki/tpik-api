package main

import (
	"database/sql"
	"time"
)

type Vacancy struct {
	ID               int32          `json:"id"`
	Url              string         `json:"url"`
	Header           string         `json:"header"`
	Source           string         `json:"source"`
	Ts               time.Time      `json:"ts"`
	Lang             sql.NullString `json:"lang"`
	RankSimple       float32        `json:"rank_simple"`
	HeadlineSimple   string         `json:"headline_simple"`
	RankFinnish      float32        `json:"rank_finnish"`
	HeadlineFinnish  string         `json:"headline_finnish"`
	RankEnglish      float32        `json:"rank_english"`
	HeadlineEnglish  string         `json:"headline_english"`
}
